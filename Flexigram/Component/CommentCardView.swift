//
//  CommentCardView.swift
//  Flexigram
//
//  Created by rafiul hasan on 27/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct CommentCardView: View {
    var comment: CommentModel
    
    var body: some View {
        HStack {
            WebImage(url: URL(string: comment.profile))
                .resizable()
                .aspectRatio(contentMode: .fill)
                .clipShape(Circle())
                .frame(width: 30, height: 30, alignment: .center)
                .shadow(color: .gray ,radius: 3)
                .padding(.leading)
            
            VStack(alignment: .leading) {
                Text(comment.username)
                    .font(.subheadline)
                Text(comment.comment)
                    .font(.caption)
            }
            
            Spacer()
            
            //Text((Date(timeIntervalSince1970: comment.date)).timeago()+"ago").font(.subheadline)
        }
    }
}

//struct CommentCardView_Previews: PreviewProvider {
//    static var previews: some View {
//        CommentCardView()
//    }
//}
