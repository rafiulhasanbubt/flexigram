//
//  CommentInputView.swift
//  Flexigram
//
//  Created by rafiul hasan on 27/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct CommentInputView: View {
    @EnvironmentObject var session: SessionStore
    @ObservedObject var commentService = CommentService()
    @State private var text: String = ""
    
    init(post: PostModel?, postId: String?) {
        if post != nil {
            commentService.post = post
        } else {
            
        }
    }
    
    func handleInput(postId: String) {
        PostService.loadPost(postId: postId) { post in
            self.commentService.post = post
        }
    }
    
    func sendComment() {
        if !text.isEmpty {
            commentService.addComment(comment: text) {
                self.text = ""
            }
        }
    }
    
    var body: some View {
        HStack {
            WebImage(url: URL(string: session.session!.profileImageUrl)!)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .scaledToFit()
                .clipShape(Circle())
                .frame(width: 60, height: 60, alignment: .center)
                .shadow(color: .gray ,radius: 3)
                .padding(.leading)
            
            HStack {
                TextEditor(text: $text)
                    .frame(height: 50)
                    .padding(4)
                    .background(RoundedRectangle(cornerRadius: 8, style: .circular).stroke(Color.black, lineWidth: 1))
                
                Button(action: {sendComment()}) {
                    Image(systemName: "paperplane")
                        .imageScale(.large).padding(.trailing)
                }
            }
        }
    }
}

//struct CommentInputView_Previews: PreviewProvider {
//    static var previews: some View {
//        CommentInputView()
//    }
//}
