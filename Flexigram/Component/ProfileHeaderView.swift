//
//  ProfileHeaderView.swift
//  Flexigram
//
//  Created by rafiul hasan on 26/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProfileHeaderView: View {
    var user: User?
    var postCount: Int
    @Binding var following: Int
    @Binding var followers: Int
    
    var body: some View {
        HStack {
            VStack {
                if user != nil {
                    WebImage(url: URL(string: user!.profileImageUrl)!)
                        .resizable()
                        .scaledToFit()
                        .clipShape(Circle())
                        .frame(width: 100, height: 100, alignment: .trailing)
                        .padding(.leading)
                } else {
                    Color.init(red: 0.9, green: 0.9, blue: 0.9)
                        .frame(width: 100, height: 100, alignment: .trailing)
                        .padding(.leading)
                }
                
                Text(user!.username)
                    .font(.headline)
                    .bold()
                    .padding(.leading)
            }
            
            VStack {
                HStack {
                    Spacer()
                    VStack{
                        Text("Posts").font(.headline)
                        Text("\(postCount)").font(.title).bold()
                    }
                    Spacer()
                    VStack{
                        Text("Followers").font(.headline)
                        Text("\(followers)").font(.title).bold()
                    }
                    
                    Spacer()
                    VStack{
                        Text("Following").font(.headline)
                        Text("\(following)").font(.title).bold()
                    }
                    Spacer()
                }
            }
        }
        .padding(.top, 40)
    }
}

//struct ProfileHeaderView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileHeaderView()
//    }
//}
