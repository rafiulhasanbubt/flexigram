//
//  FollowButtonView.swift
//  Flexigram
//
//  Created by rafiul hasan on 27/9/21.
//

import SwiftUI

struct FollowButtonView: View {
    @ObservedObject var followService = FollowService()
    @Binding var followingCount: Int
    @Binding var followersCount: Int
    @Binding var followCheck: Bool
    
    var user: User
    
    init(user: User, followCheck: Binding<Bool>, followingCount: Binding<Int>, followersCount: Binding<Int>) {
        self.user = user
        self._followCheck = followCheck
        self._followingCount = followingCount
        self._followersCount = followersCount
    }
    
    func follow() {
        if !self.followCheck {
            followService.follow(userId: user.uid) { followingCount in
                self.followingCount = followingCount
            } followersCount: { followersCount in
                self.followersCount = followersCount
            }
            self.followCheck = true
        } else {
            followService.unfollow(userId: user.uid) { followingCount in
                self.followingCount = followingCount
            } followersCount: { followersCount in
                self.followersCount = followersCount
            }
            self.followCheck = false
        }
    }
    
    var body: some View {
        Button(action: {follow()}) {
            Text((self.followCheck) ? "Unfollow":"Follow")
                .modifier(ButtonModifiers())
        }

    }
}

//struct FollowButtonView_Previews: PreviewProvider {
//    static var previews: some View {
//        FollowButtonView()
//    }
//}
