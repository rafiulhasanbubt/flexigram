//
//  PostService.swift
//  Flexigram
//
//  Created by rafiul hasan on 25/9/21.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseFirestore

class PostService {
    static var Posts = AuthService.storeRoot.collection("posts")
    static var AllPosts = AuthService.storeRoot.collection("allPosts")
    static var TimeLine = AuthService.storeRoot.collection("timeLine")
    
    static func PostsUserId(userId: String) -> DocumentReference {
        return Posts.document(userId)
    }
    
    static func timeLineUserId(userId: String) -> DocumentReference {
        return TimeLine.document(userId)
    }
    
    static func upoloadPost(caption: String, imageData: Data, onSuccess: @escaping() -> Void, onError: @escaping(_ errorMessage: String) -> Void) {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let postId = PostService.PostsUserId(userId: userId).collection("posts").document().documentID
        let storagePostRef = StorageService.storagePostId(postId: postId)
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        
        StorageService.savePostPhoto(userId: userId, caption: caption, postId: postId, imageData: imageData, metaData: metaData, storagePostRef: storagePostRef, onSuccess: onSuccess, onError: onError)
    }
    
    //
    static func loadPost(postId: String, onSuccess: @escaping(_ post: PostModel) -> Void) {
        PostService.AllPosts.document(postId).getDocument { snapshot, error in
            guard let snap = snapshot else { return }
            let dict = snap.data()
            guard let decoded = try? PostModel(fromDictionary: dict!) else { return }
            onSuccess(decoded)
        }
    }
    
    static func loadUserPosts(userId: String, onSuccess: @escaping(_ posts:([PostModel])) -> Void) {
        PostService.PostsUserId(userId: userId).collection("posts").getDocuments { snapshot, error in
            guard let snap = snapshot else { return }
            var posts = [PostModel]()
            
            for doc in snap.documents {
                let dict = doc.data()
                guard let decoder = try? PostModel.init(fromDictionary: dict) else { return }
                posts.append(decoder)
            }
            onSuccess(posts)
        }
    }
}
