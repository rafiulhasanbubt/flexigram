//
//  SearchService.swift
//  Flexigram
//
//  Created by rafiul hasan on 26/9/21.
//

import Foundation
import FirebaseAuth

class SearchService {
    static func searchUser(input: String, onSuccess: @escaping(_ user: [User]) -> Void) {
        AuthService.storeRoot.collection("users").whereField("searchname", arrayContains: input.lowercased().removeWhiteSpace()).getDocuments { snapshot, error in
            guard let snap = snapshot else { return }
            var user = [User]()
            for document in snap.documents {
                let dict = document.data()
                guard let decoded = try? User.init(fromDictionary: dict) else { return }
                if decoded.uid != Auth.auth().currentUser!.uid {
                    user.append(decoded)
                }
            }
            onSuccess(user)
        }
    }
}
