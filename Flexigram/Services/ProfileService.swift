//
//  ProfileService.swift
//  Flexigram
//
//  Created by rafiul hasan on 26/9/21.
//

import Foundation
import FirebaseFirestore
import FirebaseAuth

class ProfileService: ObservableObject {
    @Published var posts: [PostModel] = []
    @Published var isLoading = false
    @Published var following = 0
    @Published var followers = 0
    @Published var followCheck = false
    
    func loadUserPosts(userId: String) {
        PostService.loadUserPosts(userId: userId) { posts in
            self.posts = posts
        }
        
        following(userId: userId)
        followers(userId: userId)
    }
    
    //followings
    static var following = AuthService.storeRoot.collection("following")
    static func followingCollection(userId: String) -> CollectionReference {
        return following.document(userId).collection("following")
    }
    
    func following(userId: String) {
        ProfileService.followingCollection(userId: userId).getDocuments { snapshot, error in
            if let doc = snapshot?.documents {
                self.following = doc.count
            }
        }
    }
    
    static func followingId(userId: String) -> DocumentReference {
        return following.document(Auth.auth().currentUser!.uid).collection("following").document(userId)
    }
    
    //followers
    static var followers = AuthService.storeRoot.collection("followers")
    static func followersCollection(userId: String) -> CollectionReference {
        return following.document(userId).collection("followers")
    }
            
    func followers(userId: String) {
        ProfileService.followersCollection(userId: userId).getDocuments { snapshot, error in
            if let doc = snapshot?.documents {
                self.followers = doc.count
            }
        }
    }
    
    static func followersId(userId: String) -> DocumentReference {
        return followers.document(userId).collection("followers").document(Auth.auth().currentUser!.uid)
    }
    
    func followState(userId: String) {
        ProfileService.followingId(userId: userId).getDocument { document, error in
            if let doc = document, doc.exists {
                self.followCheck = true
            } else {
                self.followCheck = false
            }
        }
    }
}
