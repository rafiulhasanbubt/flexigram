//
//  CommentModel.swift
//  Flexigram
//
//  Created by rafiul hasan on 27/9/21.
//

import Foundation

struct CommentModel: Codable, Identifiable {
    var id = UUID()
    var profile: String
    var postId: String
    var username: String
    var date: Double
    var comment: String
    var ownerId: String
}
