//
//  ChatModel.swift
//  Flexigram
//
//  Created by rafiul hasan on 28/9/21.
//

import Foundation
import Firebase

struct Chat: Codable, Hashable {
    var messageId: String
    var textMessage: String
    var profile: String
    var photoUrl: String
    var sender: String
    var username: String
    var timestamp: Double
    var isPhoto: Bool
    var isCurrentUser: Bool {
        return Auth.auth().currentUser!.uid == sender
    }
}
