//
//  UserModel.swift
//  Flexigram
//
//  Created by rafiul hasan on 8/9/21.
//

import Foundation

struct User: Encodable, Decodable {
    var uid: String
    var email: String
    var profileImageUrl: String
    var username: String
    var searchname: [String]
    var bio: String
}
