//
//  MessageModel.swift
//  Flexigram
//
//  Created by rafiul hasan on 28/9/21.
//

import Foundation

struct Message: Codable, Identifiable {
    var id = UUID()
    var lastMessage: String
    var username: String
    var isPhoto: Bool
    var timestamp: Double
    var userId: String
    var profile: String
}
