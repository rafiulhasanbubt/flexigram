//
//  FlexigramApp.swift
//  Flexigram
//
//  Created by rafiul hasan on 8/9/21.
//

import SwiftUI
import Firebase

@main
struct FlexigramApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(SessionStore())
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        print("Firebase")
        FirebaseApp.configure()
        return true
    }
}
