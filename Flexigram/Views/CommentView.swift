//
//  CommentView.swift
//  Flexigram
//
//  Created by rafiul hasan on 27/9/21.
//

import SwiftUI

struct CommentView: View {
    @StateObject var commentService = CommentService()
    var post: PostModel?
    var postId: String?
    
    var body: some View {
        VStack {
            ScrollView {
                if !commentService.comments.isEmpty {
                    ForEach(commentService.comments) { comment in
                        CommentCardView(comment: comment)
                    }
                }
            }
            CommentInputView(post: post, postId: postId)
        }
        .navigationTitle("Comments")
        .onAppear {
            self.commentService.postId = self.post == nil ? self.postId : self.post?.postId
            self.commentService.loadComment()
        }
        .onDisappear {
            if self.commentService.listener != nil {
                self.commentService.listener.remove()
            }
        }
    }
}

//struct CommentView_Previews: PreviewProvider {
//    static var previews: some View {
//        CommentView()
//    }
//}
