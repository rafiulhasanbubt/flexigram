//
//  WelcomeView.swift
//  Flexigram
//
//  Created by rafiul hasan on 25/9/21.
//

import SwiftUI
import FirebaseAuth

struct WelcomeView: View {
    @EnvironmentObject var session: SessionStore
    @StateObject var profileService = ProfileService()
    
    var body: some View {
        ScrollView {
            VStack {
                ForEach(self.profileService.posts, id:\.postId) { post in
                    PostCardImageView(post: post)
                    PostCardView(post: post)
                }
            }
        }
        .navigationTitle("Home")
        .navigationBarTitleDisplayMode(.inline)
        .onAppear {
            self.profileService.loadUserPosts(userId:Auth.auth().currentUser!.uid)
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
