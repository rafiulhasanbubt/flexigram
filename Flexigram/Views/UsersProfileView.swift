//
//  UsersProfileView.swift
//  Flexigram
//
//  Created by rafiul hasan on 27/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct UsersProfileView: View {
    @StateObject var profileService = ProfileService()
    @State private var selection = 1
    
    let threeColumns = [GridItem(),GridItem(),GridItem()]
    var user: User
    
    var body: some View {
        ScrollView {
            ProfileHeaderView(user: user, postCount: profileService.posts.count, following: $profileService.following, followers: $profileService.followers)
            HStack {
                FollowButtonView(user: user, followCheck: $profileService.followCheck, followingCount: $profileService.following, followersCount: $profileService.followers)
            }.padding(.horizontal)
            
            Picker("", selection: $selection) {
                Image(systemName: "circle.grid.2x2.fill").tag(0)
                Image(systemName: "person.fill").tag(1)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding(.horizontal)
            
            if selection == 0 {
                LazyVGrid(columns: threeColumns) {
                    ForEach(self.profileService.posts, id: \.postId) { post in
                        WebImage(url: URL(string: post.mediaUrl)!)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: UIScreen.main.bounds.width/3, height: UIScreen.main.bounds.width/3)
                    }
                }
            } else {
                VStack {
                    ForEach(self.profileService.posts, id:\.postId) { post in
                        PostCardImageView(post: post)
                        PostCardView(post: post)
                    }
                }
            }
        }
        .navigationTitle(Text(self.user.username))
        .onAppear {
            self.profileService.loadUserPosts(userId: self.user.uid)
        }
    }
}

//struct UsersProfileView_Previews: PreviewProvider {
//    static var previews: some View {
//        UsersProfileView()
//    }
//}
