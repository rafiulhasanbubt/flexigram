//
//  ProfileView.swift
//  Flexigram
//
//  Created by rafiul hasan on 25/9/21.
//

import SwiftUI
import FirebaseAuth
import SDWebImageSwiftUI

struct ProfileView: View {
    @EnvironmentObject var session: SessionStore
    @State private var selection = 1
    @StateObject var profileService = ProfileService()
    @State private var isLinkActive = false
    
    let threeColumns = [GridItem(),GridItem(),GridItem()]
    
    func listen() {
        session.listen()
    }
    
    var body: some View {
        ScrollView {
            VStack {
                ProfileHeaderView(user: self.session.session, postCount: profileService.posts.count, following: $profileService.following, followers: $profileService.followers)
                
                VStack(alignment: .leading) {
                    Text(session.session?.bio ?? "").font(.headline).lineLimit(1)
                }
                NavigationLink(destination: EditProfileView(session: self.session.session), isActive: $isLinkActive) {
                Button(action: {self.isLinkActive = true}) {
                    Text("Edit Profile")
                        .font(.title).modifier(ButtonModifiers())
                }
                .padding(.horizontal)
            }
                
                Picker("", selection: $selection) {
                    Image(systemName: "circle.grid.2x2.fill").tag(0)
                    Image(systemName: "person.fill").tag(1)
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding(.horizontal)
                
                if selection == 0 {
                    LazyVGrid(columns: threeColumns) {
                        ForEach(self.profileService.posts, id: \.postId) { post in
                            WebImage(url: URL(string: post.mediaUrl)!)
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: UIScreen.main.bounds.width/3, height: UIScreen.main.bounds.width/3)
                        }
                    }
                } else if (self.session.session == nil) {
                        Text("")
                    } else {
                    VStack {
                        ForEach(self.profileService.posts, id:\.postId) { post in
                            PostCardImageView(post: post)
                            PostCardView(post: post)
                        }
                    }
                }
            }
            .navigationTitle("Profile")
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(
                leading: NavigationLink(destination: UserProfileView()) { Image(systemName: "person.fill") },
                trailing: Button(action: {session.logout()}){ Image(systemName: "arrow.right.circle.fill")})
            .onAppear {
                self.profileService.loadUserPosts(userId:Auth.auth().currentUser!.uid)
            }
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
