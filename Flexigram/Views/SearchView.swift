//
//  SearchView.swift
//  Flexigram
//
//  Created by rafiul hasan on 25/9/21.
//

import SwiftUI

struct SearchView: View {
    @Binding var searchUser: String
    @State private var isSearching = false
    
    var body: some View {
        VStack {
            HStack {
                TextField("Search user here", text: $searchUser)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .padding(.leading)
            }
            .padding()
            .background(Color(.systemGray5))
            .cornerRadius(6)
            .padding(.horizontal)
            .onTapGesture {
                isSearching = true
            }
            .overlay(
                HStack {
                    Image(systemName: "magnifyingglass")
                    Spacer()
                    Button(action: {searchUser = ""}) {
                        Image(systemName: isSearching == true ? "xmark.circle" : "doc.text.magnifyingglass")
                    }
                }
                .padding(.horizontal, 28)
                .foregroundColor(.gray)
            )
        }.navigationTitle("Search")
        .navigationBarTitleDisplayMode(.inline)
    }
}
    
//struct Search_Previews: PreviewProvider {
//    static var previews: some View {
//        SearchView(searchUser: .constant("Search"))
//    }
//}
