//
//  AudioView.swift
//  Flexigram
//
//  Created by rafiul hasan on 13/9/21.
//

import SwiftUI
import AVKit

struct AudioView: View {
    @State var toggleRecordStop = "Record"
    @State var scaleBigger = 0.5
    @State var scaleMedium = 0.5
    @State var scaleSmaller = 0.5
    @State var counter = Text("00.00.00")
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .frame(width: 120, height: 120)
                    .scaleEffect(CGFloat(scaleBigger))
                    .foregroundColor(Color(.systemGray6))
                    .animation(Animation.easeOut(duration: 1).repeatForever(autoreverses: true))
                Circle()
                    .frame(width: 100, height: 100)
                    .scaleEffect(CGFloat(scaleMedium))
                    .foregroundColor(Color(.systemGray5))
                    .animation(Animation.easeOut(duration: 1).repeatForever(autoreverses: true))
                Circle()
                    .frame(width: 80, height: 80)
                    .scaleEffect(CGFloat(scaleSmaller))
                    .foregroundColor(Color(.systemGray4))
                    .animation(Animation.easeOut(duration: 1).repeatForever(autoreverses: true))
                Circle()
                    .frame(width: 60, height: 60)
                    .foregroundColor(Color(.systemRed))
                    .overlay(Text(toggleRecordStop)
                                .font(.system(size: 12)))
            }
            .onTapGesture {
                toggleRecordStop = "stop"
                scaleBigger = 1.0
                scaleMedium = 1.0
                scaleSmaller = 1.0
                counter = Text(Date().addingTimeInterval(0.0), style: .timer)
                    .font(.system(size: 16))
                    .fontWeight(.ultraLight)
            }
            counter
                .font(.system(size: 16))
                .fontWeight(.ultraLight)
        }
        .padding()
    }
}

struct AudioView_Previews: PreviewProvider {
    static var previews: some View {
        AudioView()
    }
}
