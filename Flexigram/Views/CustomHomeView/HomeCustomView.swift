//
//  HomeCustomView.swift
//  Flexigram
//
//  Created by rafiul hasan on 25/9/21.
//

import SwiftUI
import MapKit

struct HomeCustomView: View {
    @ObservedObject var locationManager = LocationManager()
    @State private var landmarks: [Landmark] = [Landmark]()
    @State private var search: String = ""
    
    private func getNearByLandmarks() {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = search
        
        let search = MKLocalSearch(request: request)
        search.start { response, error in
            if let response = response {
                let mapItems = response.mapItems
                self.landmarks = mapItems.map {
                    Landmark(placemark: $0.placemark)
                }
            }
        }
    }
    
    var body: some View {
        ZStack(alignment: .top) {
            MapView(landmarks: landmarks)
                .ignoresSafeArea(.all)
            
            VStack {
                HStack(alignment: .center) {
                    Image(systemName: "list.dash")
                    TextField("search", text: $search, onEditingChanged: {_ in}) {
                        self.getNearByLandmarks()
                    }.textFieldStyle(RoundedBorderTextFieldStyle())
                    
                    Image(systemName: "mic")
                    Image(systemName: "arrow.left.arrow.right.square")
                }
                .background(Color.white)
                .offset(y:30)
                .padding()
                
                Spacer()
                
                AudioView()
                    .padding(.bottom, 10)
                CustomTabView()
            }
        }
    }
}

struct HomeCustomView_Previews: PreviewProvider {
    static var previews: some View {
        HomeCustomView()
    }
}
