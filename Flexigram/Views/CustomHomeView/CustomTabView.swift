//
//  CustomTabView.swift
//  Flexigram
//
//  Created by rafiul hasan on 13/9/21.
//

import SwiftUI

struct CustomTabView: View {
    var body: some View {
        HStack(alignment: .center) {
            Spacer()
            Image(systemName: "star.square")
                .font(.system(size: 36))
                .frame(width: 36, height: 36)
            Spacer()
            Image(systemName: "bell.fill")
                .font(.system(size: 36))
                .frame(width: 36, height: 36)
            Spacer()
            Button(action: {}, label: {
                Text("Logout")
            })
            .font(.system(size: 16))
            .frame(width: 60, height: 36)
            Spacer()
        }
        .frame(maxWidth: .infinity, maxHeight: 60)
        .background(Color.white)
        .border(Color.gray, width: 1)
    }
}

struct CustomTabView_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabView()
    }
}
