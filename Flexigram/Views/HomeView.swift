//
//  HomeView.swift
//  Flexigram
//
//  Created by rafiul hasan on 11/9/21.
//

import SwiftUI
import MapKit

struct HomeView: View {
    @EnvironmentObject var session: SessionStore
    
    var body: some View {
        VStack {
            CustomTab()
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

var tabs = ["house.fill", "magnifyingglass", "camera.viewfinder", "heart.fill", "person.fill"]

struct CustomTab: View {
    @State var searchUser = ""
    @State var selectedTab = "house.fill"
    @State var edge = UIApplication.shared.windows.first?.safeAreaInsets
    
    var body: some View {
        ZStack(alignment: Alignment(horizontal: .center, vertical: .bottom)) {
            NavigationView {
                TabView(selection: $selectedTab) {
                    WelcomeView()
                        .tag("house.fill")
                    SearchView(searchUser: $searchUser)
                        .tag("magnifyingglass")
                    PostView()
                        .tag("camera.viewfinder")
                    NotificationsView()
                        .tag("heart.fill")
                    ProfileView()
                        .tag("person.fill")
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                .ignoresSafeArea(.all, edges: .bottom)
            }
            .accentColor(.pink)
            
            HStack(spacing: 0) {
                ForEach(tabs, id: \.self) { image in
                    TabButton(image: image, selectedTab: $selectedTab)
                    if image != tabs.last {
                        Spacer(minLength: 0)
                    }
                }
            }
            .padding(.horizontal, 25)
            .padding(.vertical, 5)
            .background(Color.white)
            .clipShape(Capsule())
            .shadow(color: .black.opacity(0.15), radius: 5, x: 5, y: 5)
            .shadow(color: .black.opacity(0.15), radius: 5, x: -5, y: -5)
            .padding(.horizontal)
            .padding(.bottom, edge!.bottom == 0 ?  20: 0)
        }
        .ignoresSafeArea(.keyboard, edges: .bottom)
        .background(Color.black.opacity(0.05).ignoresSafeArea(.all, edges: .all))
    }
}

struct TabButton: View {
    var image: String
    @Binding var selectedTab: String
    
    var body: some View {
        Button(action: {selectedTab = image}) {
            Image(systemName: "\(image)")
                .foregroundColor(selectedTab == image ? Color.gray: Color.black)
                .padding()
        }
    }
}
