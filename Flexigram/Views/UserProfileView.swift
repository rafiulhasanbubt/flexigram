//
//  UserProfileView.swift
//  Flexigram
//
//  Created by rafiul hasan on 27/9/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct UserProfileView: View {
    @State private var value: String = ""
    @State var user: [User] = []
    @State var isLoading = false
    
    func searchUsers() {
        isLoading = true
        SearchService.searchUser(input: value) { users in
            self.isLoading = false
            self.user = users
        }
    }
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                SearchView(searchUser: $value)
                    .padding()
                    .onChange(of: value, perform: { value in
                        searchUsers()
                    })
                
                if !isLoading {
                    ForEach(user, id: \.uid) { user in
                        NavigationLink(destination: UsersProfileView(user: user)) {
                            HStack(spacing: 10) {
                                WebImage(url: URL(string: user.profileImageUrl)!)
                                    .resizable()
                                    .scaledToFit()
                                    .clipShape(Circle())
                                    .frame(width: 60, height: 60, alignment: .trailing)
                                
                                VStack (alignment: .leading, spacing: 8) {
                                    Text("\(user.username)")
                                        .font(.body)
                                    Text("\(user.email)")
                                        .font(.body)
                                }
                            }
                        }
                        Divider().background(Color.gray)
                    }
                }
            }
        }.navigationTitle("Search user")
    }
}

struct UserProfileView_Previews: PreviewProvider {
    static var previews: some View {
        UserProfileView()
    }
}
